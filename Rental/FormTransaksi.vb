﻿Imports System.Data.SqlClient
Public Class FormTransaksi

    Dim totalbiaya As Integer

    Private Sub FormTransaksi_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim perintah As New SqlCommand
        Dim data As SqlDataReader
        'Dim totalbiaya As Integer

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        perintah.CommandText = "select max(IDTransaksi) from TTransaksi"
        data = perintah.ExecuteReader
        If data.Read Then
            txt_idtransaksi.Text = data(0) + 1
        End If

        txt_tanggal.Text = Format(Today, "dd-MMMM-yyy")

        With ListViewKoleksi
            .Columns.Add("ID Koleksi", 80, HorizontalAlignment.Center)
            .Columns.Add("Judul Koleksi", 250, HorizontalAlignment.Center)
            .Columns.Add("Jenis Koleksi", 100, HorizontalAlignment.Center)
            .Columns.Add("Tgl Kembali", 120, HorizontalAlignment.Center)
            .Columns.Add("Harga Sewa", 80, HorizontalAlignment.Center)
        End With

        totalbiaya = 0
        txt_totalbiaya.Text = totalbiaya

        PBfoto.ImageLocation = ("F:\Tasks\VB Project\Rental\image\default.png")
        PBfoto.SizeMode = PictureBoxSizeMode.StretchImage
    End Sub

    Private Sub txt_idanggota_Leave(sender As Object, e As EventArgs) Handles txt_idanggota.Leave
        Dim perintah, perintah1 As New SqlCommand
        Dim data, data1 As SqlDataReader
        Dim idanggota, hasilpesan As String

        idanggota = txt_idanggota.Text

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand
        perintah.CommandText = "select Nama, Foto from TAnggota where IDAnggota = '" & idanggota & "'"

        Dim MyFoto As Byte()
        data = perintah.ExecuteReader
        If data.Read Then
            txt_nama.Text = Trim(data(0))
            MyFoto = data(1)
            PBfoto.Image = Image.FromStream(New IO.MemoryStream(MyFoto))
            PBfoto.SizeMode = PictureBoxSizeMode.StretchImage
        Else
            txt_nama.Text = "Tidak Tersedia"
            PBfoto.BackgroundImage = Image.FromFile("F:\Tasks\VB Project\Rental\image\default.png")
            PBfoto.SizeMode = PictureBoxSizeMode.StretchImage
            txt_idanggota.Focus()
        End If
        koneksiSQL.Close()

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah1 = koneksiSQL.CreateCommand
        perintah1.CommandText = "SELECT COUNT(Tr.IDTransaksi) FROM TTransaksi Tr, TDetilTransaksi TD WHERE Tr.IDAnggota like '%" & idanggota & "%' AND Tr.IDTransaksi = TD.IDTransaksi AND TD.TglPengembalian is Null"

        data1 = perintah1.ExecuteReader
        If idanggota <> "" Then
            If data1.Read And data1(0) > 0 Then
                hasilpesan = MsgBox("" & txt_nama.Text & " masih mempunyai pinjaman sebanyak : " & data1(0) & " koleksi, apakah proses pinjaman dilanjutkan?", MsgBoxStyle.YesNo, "Informasi")
                If hasilpesan = vbNo Then
                    txt_idanggota.Text = ""
                    txt_nama.Text = "Nama Anggota"
                    txt_idanggota.Focus()
                End If
            End If
        End If
        koneksiSQL.Close()
    End Sub

    Private Sub txt_idkoleksi_Leave(sender As Object, e As EventArgs) Handles txt_idkoleksi.Leave
        Dim perintah, perintah1 As New SqlCommand
        Dim data As SqlDataReader
        Dim idkoleksi As String

        idkoleksi = txt_idkoleksi.Text

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand
        perintah.CommandText = "select Judul from TKoleksi where IDKoleksi = '" & idkoleksi & "'"

        data = perintah.ExecuteReader
        If data.Read Then
            txt_judul.Text = Trim(data(0))
        Else
            txt_judul.Text = "Tidak Tersedia"
            txt_idkoleksi.Focus()
        End If
        koneksiSQL.Close()

        If txt_idkoleksi.Text = "" Or txt_judul.Text = "Tidak Tersedia" Then
            btn_masuklist.Enabled = False
        Else
            btn_masuklist.Enabled = True
        End If
    End Sub

    Private Sub btn_masuklist_Click(sender As Object, e As EventArgs) Handles btn_masuklist.Click
        Dim perintah As New SqlCommand
        Dim data As SqlDataReader
        Dim idkoleksi, judul, jenis As String
        Dim harga As Integer
        Dim tanggal As String

        tanggal = Format(DateAdd(DateInterval.Day, 5, Today), "dd - MMMM - yyy")
        idkoleksi = txt_idkoleksi.Text
        judul = txt_judul.Text
        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()

        perintah = koneksiSQL.CreateCommand
        perintah.CommandText = "select Judul, Jenis, BiayaSewa from TKoleksi where IDKoleksi = '" & idkoleksi & "'"

        data = perintah.ExecuteReader
        If data.Read() Then
            judul = Trim(data(0))
            jenis = Trim(data(1))
            harga = data(2)
        End If
        Dim lv As ListViewItem

        lv = ListViewKoleksi.Items.Add(idkoleksi)
        lv.SubItems.Add(judul)
        lv.SubItems.Add(jenis)
        lv.SubItems.Add(tanggal)
        lv.SubItems.Add(harga)
        totalbiaya = totalbiaya + harga
        txt_totalbiaya.Text = totalbiaya 'Totalbiaya harus dideklarasikan sebagai variable global
        txt_idkoleksi.Text = ""
        txt_judul.Text = ""
        txt_idkoleksi.Focus()
    End Sub

    Private Sub btn_keluarlist_Click(sender As Object, e As EventArgs) Handles btn_keluarlist.Click
        ListViewKoleksi.BeginUpdate()
        Dim i, subBiaya As Integer

        For i = ListViewKoleksi.Items.Count - 1 To 0 Step -1
            If ListViewKoleksi.Items(i).Checked Then
                subBiaya = ListViewKoleksi.Items.Item(i).SubItems(4).Text 'SubItem Bukan 2 Tapi 4 
                totalbiaya = totalbiaya - subBiaya
                txt_totalbiaya.Text = totalbiaya

                ListViewKoleksi.Items.RemoveAt(i)
                btn_masuklist.Enabled = True
            End If
        Next i
        ListViewKoleksi.EndUpdate()
    End Sub

    Private Sub btn_proses_Click(sender As Object, e As EventArgs) Handles btn_proses.Click
        Dim idtransaksi, idanggota, tglpinjam As String
        Dim totalbiayasewa As Integer
        Dim perintah As SqlCommand

        idtransaksi = txt_idtransaksi.Text
        idanggota = txt_idanggota.Text
        tglpinjam = Format(Today, "yyy-MM-dd")

        totalbiayasewa = txt_totalbiaya.Text
        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()

        perintah = koneksiSQL.CreateCommand
        perintah.CommandText = "insert into TTransaksi(IDTransaksi, IDAnggota, TglPinjam, IDUser, TotBiayaSewa) values(" & idtransaksi & ", '" & idanggota & "', '" & tglpinjam & "', 1, " & totalbiayasewa & ")" 'IDUser dibuat manual aja dulu
        perintah.ExecuteNonQuery()

        Dim i As Integer
        Dim idkoleksi As String
        Dim tglharuskembali As Date
        tglharuskembali = DateAdd("d", 5, Today)

        For i = 0 To ListViewKoleksi.Items.Count - 1
            idkoleksi = ListViewKoleksi.Items.Item(i).SubItems(0).Text
            perintah.CommandText = "insert into TDetilTransaksi(IDTransaksi, IDKoleksi, TglHrsKembali) values(" & idtransaksi & ", '" & idkoleksi & "', '" & Format(tglharuskembali, "yyy-MM-dd") & "')"
            perintah.ExecuteNonQuery()
            perintah.CommandText = "update TKoleksi set jumlah=jumlah-1 where IDKoleksi = '" & idkoleksi & "'"
            perintah.ExecuteNonQuery()
        Next

        MsgBox("Transaksi berhasil!", MsgBoxStyle.Information, "Informasi")

        'Dim hasilpesan As String

        'hasilpesan = MsgBox("Data berhasil ditambahkan, cetak nota?", MsgBoxStyle.YesNo + MsgBoxStyle.Information, "Informasi")
        'If hasilpesan = vbYes Then
        '    Dim formstruk As New FormStruk
        '    formstruk.txt_idtransaksai.text = idtransaksi
        '    formstruk.ShowDialog()
        'End If

        koneksiSQL.Close()
        Me.Close()
    End Sub

    Private Sub btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click
        ProsesClear()
    End Sub

    Private Sub ProsesClear()
        Dim i, jml As Int16
        If ListViewKoleksi.Items.Count > 0 Then
            jml = ListViewKoleksi.Items.Count - 1
            For i = jml To 0 Step -1
                ListViewKoleksi.Items.RemoveAt(i)
            Next i
            txt_idanggota.Text = ""
            txt_nama.Text = ""
            txt_idkoleksi.Text = ""
            txt_judul.Text = ""
            txt_totalbiaya.Text = ""
        End If
    End Sub

End Class