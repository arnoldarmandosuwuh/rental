﻿Imports System.Data.SqlClient
Imports System.IO

Public Class FormUser

    Private Sub FormUser_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim perintah As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        perintah.CommandText = "select * from TUser"

        da.SelectCommand = perintah
        da.Fill(ds, "tuser")

        dgv_user.DataSource = ds
        dgv_user.DataMember = "tuser"
        dgv_user.ReadOnly = True
        'DGVAnggota.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        'DGVAnggota.AutoResizeColumn()
        dgv_user.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        PBFoto.ImageLocation = ("F:\Tasks\VB Project\Rental\image\default.png")
        PBFoto.SizeMode = PictureBoxSizeMode.StretchImage

        koneksiSQL.Close()
    End Sub

    Private Sub btn_insert_Click(sender As Object, e As EventArgs) Handles btn_insert.Click
        Dim iduser, namauser, password, repassword, status As String
        Dim perintah As New SqlCommand

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        iduser = txt_iduser.Text
        namauser = txt_namauser.Text
        password = txt_password.Text
        repassword = txt_repassword.Text
        status = cb_status.SelectedItem

        If password = repassword Then
            perintah.CommandText = "insert into TUser values ('" & iduser & "', '" & namauser & "', '" & password & "', '" & status & "', @gambar)"
            Dim ms As New MemoryStream
            PBFoto.Image.Save(ms, PBFoto.Image.RawFormat)

            perintah.Parameters.Add("@gambar", SqlDbType.Image).Value = ms.ToArray()

            'perintah.ExecuteNonQuery()

            If perintah.ExecuteNonQuery > 0 Then
                MsgBox("Data telah disimpan", MsgBoxStyle.Information, "Informasi")
                resetField()
                FormUser_Load(Me, New System.EventArgs)
            Else
                MsgBox("Data gagal disimpan", MsgBoxStyle.Critical, "Error")
            End If
            koneksiSQL.Close()

            btn_insert.Enabled = True
            btn_clear.Enabled = True
            btn_update.Enabled = False
            btn_delete.Enabled = False
        Else
            MsgBox("Password tidak sesuai", MsgBoxStyle.Information, "Informasi")
        End If
    End Sub

    Private Sub resetField()
        txt_iduser.Enabled = True
        txt_iduser.Text = ""
        txt_namauser.Text = ""
        txt_password.Text = ""
        txt_repassword.Text = ""
        cb_status.SelectedItem = Nothing

        btn_insert.Enabled = True
        btn_clear.Enabled = True
        btn_update.Enabled = False
        btn_delete.Enabled = False
    End Sub

    Private Sub btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click
        resetField()
    End Sub

    Private Sub btn_delete_Click(sender As Object, e As EventArgs) Handles btn_delete.Click
        Dim repassword As String
        Dim hasilpesan As String
        Dim perintah As New SqlCommand

        repassword = txt_repassword.Text

        bariske = dgv_user.CurrentRow.Index
        idhapus = dgv_user.Item(0, bariske).Value

        hasilpesan = MsgBox("Apakah anda yakin akan menghapus " & Trim(idhapus) & "?", MsgBoxStyle.YesNo, "Perhatian")
        If hasilpesan = vbYes Then
            koneksiSQL = AmbilKoneksi()
            koneksiSQL.Open()

            perintah = koneksiSQL.CreateCommand
            perintah.CommandText = "delete from TUser where IDUser = '" & idhapus & "'"
            perintah.ExecuteNonQuery()
            resetField()

            koneksiSQL.Close()
            FormUser_Load(Me, New System.EventArgs)
        End If
    End Sub

    Private Sub dgv_user_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_user.CellClick
        Dim i As Integer

        i = dgv_user.CurrentRow.Index
        txt_iduser.Text = Trim(dgv_user.Item(0, i).Value)
        txt_namauser.Text = Trim(dgv_user.Item(1, i).Value)
        txt_password.Text = Trim(dgv_user.Item(2, i).Value)
        'txt_repassword.Text = dgv_user.Item(2, i).Value
        cb_status.Text = Trim(dgv_user.Item(3, i).Value)

        txt_iduser.Enabled = False
        btn_insert.Enabled = False
        btn_clear.Enabled = True
        btn_update.Enabled = True
        btn_delete.Enabled = True
    End Sub

    Private Sub btn_update_Click(sender As Object, e As EventArgs) Handles btn_update.Click
        Dim namauser, password, repassword, status As String
        Dim hasilpesan As String
        Dim perintah As New SqlCommand

        namauser = txt_namauser.Text
        password = txt_password.Text
        repassword = txt_repassword.Text
        status = cb_status.SelectedItem

        bariske = dgv_user.CurrentRow.Index
        idedit = dgv_user.Item(0, bariske).Value

        If repassword = "" Then
            MsgBox("Password harus benar", MsgBoxStyle.Critical, "Error")
        Else
            hasilpesan = MsgBox("Apakah anda yakin akan mengedit " & Trim(idedit) & "?", MsgBoxStyle.YesNo, "Perhatian")
            If hasilpesan = vbYes Then
                koneksiSQL = AmbilKoneksi()
                koneksiSQL.Open()

                perintah = koneksiSQL.CreateCommand
                perintah.CommandText = "update TUser set NamaUser = '" & namauser & "', Password = '" & password & "', Status = '" & status & "', Foto = @gambar where IDUser = '" & idedit & "'"
                Dim ms As New MemoryStream

                PBFoto.Image.Save(ms, PBFoto.Image.RawFormat)
                perintah.Parameters.Add("@gambar", SqlDbType.Image).Value = ms.ToArray
                perintah.ExecuteNonQuery()
                resetField()

                koneksiSQL.Close()
                FormUser_Load(Me, New System.EventArgs)
            End If
        End If
    End Sub

    Private Sub txt_iduser_Leave(sender As Object, e As EventArgs) Handles txt_iduser.Leave
        Dim iduser As String
        Dim perintah As New SqlCommand

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        iduser = txt_iduser.Text

        perintah.CommandText = "select IDUser from TUser where IDUser = '" & iduser & "'"

        Dim dr As SqlDataReader
        dr = perintah.ExecuteReader()

        If iduser = "" Then
            cekid.Checked = False
        Else
            If dr.Read Then
                cekid.Checked = False
                MsgBox("ID tersebut sudah ada", MsgBoxStyle.Critical, "Peringatan")
                txt_iduser.Focus()
            Else
                cekid.Checked = True
            End If
        End If
    End Sub

    Private Sub txt_repassword_Leave(sender As Object, e As EventArgs) Handles txt_repassword.Leave
        Dim password, repassword As String

        password = txt_password.Text
        repassword = txt_repassword.Text

        If repassword = password Then
            cekpass.Checked = True
        Else
            cekpass.Checked = False
            MsgBox("Password tidak sesuai", MsgBoxStyle.Critical, "Peringatan")
            txt_repassword.Focus()
        End If
    End Sub

    Private Sub btn_pilihfoto_Click(sender As Object, e As EventArgs) Handles btn_pilihfoto.Click

        ODBukaFoto.Filter = "All file (*.*)|*.*| JPG Files(*.jpg)|*.jpg|JPEG Files (*.jpg)|*.jpeg|PNG Files(*.png)|*.png|GIF Files(*.gif)|*.gif"
        Try
            If ODBukaFoto.ShowDialog = Windows.Forms.DialogResult.OK Then
                PBFoto.Image = New Bitmap(ODBukaFoto.FileName)
                PBFoto.SizeMode = PictureBoxSizeMode.StretchImage
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub dgv_user_Paint(sender As Object, e As PaintEventArgs) Handles dgv_user.Paint
        Dim jmlbaris As Integer
        Dim iduser As String

        jmlbaris = dgv_user.RowCount - 1
        bariske = dgv_user.CurrentCell.RowIndex

        If bariske < jmlbaris Then
            iduser = dgv_user.Item(0, bariske).Value

            Dim perintah As New SqlCommand
            Dim data As SqlDataReader

            koneksiSQL = AmbilKoneksi()
            koneksiSQL.Open()

            perintah = koneksiSQL.CreateCommand
            perintah.CommandText = "select Foto from TUser where IDUser = '" & iduser & "'"

            data = perintah.ExecuteReader
            data.Read()

            Dim myfoto As Byte()
            If Not IsDBNull(data(0)) Then
                myfoto = data(0)
                PBFoto.Image = Image.FromStream(New IO.MemoryStream(myfoto))
                PBFoto.SizeMode = PictureBoxSizeMode.StretchImage
            End If
            koneksiSQL.Close()
        Else
            PBFoto.ImageLocation = ("F:\Tasks\VB Project\Rental\image\default.png")
            PBFoto.Load()
        End If
    End Sub
    Private Sub txt_cari_TextChanged(sender As Object, e As EventArgs) Handles txt_cari.TextChanged
        Dim perintah As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim katakunci As String

        katakunci = txt_cari.Text

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        perintah.CommandText = "select * from TUser where IDUser like '%" & katakunci & "%' or NamaUser like '%" & katakunci & "%' or Password like '%" & katakunci & "%'"

        da.SelectCommand = perintah
        da.Fill(ds, "tuser")

        dgv_user.DataSource = ds
        dgv_user.DataMember = "tuser"
        dgv_user.ReadOnly = True
        dgv_user.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

        koneksiSQL.Close()
    End Sub
End Class