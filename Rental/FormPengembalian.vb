﻿Imports System.Data.SqlClient
Public Class FormPengembalian

    Dim totaldenda, totDK, subDK As Integer

    Private Sub btn_cek_Click(sender As Object, e As EventArgs) Handles btn_cek.Click
        Dim perintah As New SqlCommand
        Dim data As SqlDataReader
        Dim idanggota As String
        Dim jmlterlambat, denda, jmldenda As Integer
        Dim lv As ListViewItem

        idanggota = txt_idanggota.Text
        denda = 1000
        totaldenda = 0

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()

        perintah = koneksiSQL.CreateCommand
        perintah.CommandText = "SELECT tr.IDTransaksi, td.IDKoleksi, tk.Judul, tr.TglPinjam, td.TglHrsKembali from TTransaksi tr, TDetilTransaksi td, TKoleksi tk WHERE tr.IDAnggota like '" & idanggota & "' AND tr.IDTransaksi = td.IDTransaksi AND td.IDKoleksi = tk.IDKoleksi AND td.TglPengembalian is NULL"

        data = perintah.ExecuteReader

        While data.Read()
            lv = ListViewKoleksi.Items.Add(data(0))
            lv.SubItems.Add(data(1))
            lv.SubItems.Add(data(2))
            lv.SubItems.Add(Format(data(3), "dd-MMMM-yyy"))
            lv.SubItems.Add(Format(data(4), "dd-MMMM-yyy"))
            jmlterlambat = DateDiff("d", data(4), Today)

            If jmlterlambat < 0 Then
                lv.SubItems.Add("0")
                lv.SubItems.Add("0")
            Else
                lv.SubItems.Add(jmlterlambat)
                jmldenda = jmlterlambat * denda

                lv.SubItems.Add(jmldenda)
                totaldenda = totaldenda + jmldenda
            End If
        End While
        txt_totaldenda.Text = totaldenda
        koneksiSQL.Close()
    End Sub

    Private Sub FormPengembalian_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        With ListViewKoleksi
            .Columns.Add("ID Transaksi", 80, HorizontalAlignment.Center)
            .Columns.Add("ID Koleksi", 80, HorizontalAlignment.Center)
            .Columns.Add("Judul Koleksi", 200, HorizontalAlignment.Center)
            .Columns.Add("Tgl Pinjam", 80, HorizontalAlignment.Center)
            .Columns.Add("Tgl Harus Kembali", 80, HorizontalAlignment.Center)
            .Columns.Add("Terlambat", 80, HorizontalAlignment.Center)
            .Columns.Add("Denda", 80, HorizontalAlignment.Center)
        End With
    End Sub

    Private Sub cb_semua_CheckedChanged(sender As Object, e As EventArgs) Handles cb_semua.CheckedChanged
        ListViewKoleksi.BeginUpdate()

        Dim i, jmlkoleksikembali As Integer

        If cb_semua.Checked Then
            For i = ListViewKoleksi.Items.Count - 1 To 0 Step -1
                ListViewKoleksi.Items(i).Checked = True
            Next
        Else
            For i = ListViewKoleksi.Items.Count - 1 To 0 Step -1
                ListViewKoleksi.Items(i).Checked = False
            Next
        End If
        ListViewKoleksi.EndUpdate()
    End Sub

    Private Sub btn_proses_Click(sender As Object, e As EventArgs) Handles btn_proses.Click
        Dim sql1, sql2, sql3, idtraksaksikembali, idkoleksikembali As String
        Dim tglkembali As String
        Dim perintah As New SqlCommand

        tglkembali = Format(Today, "MM-dd-yyy")

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()

        totDK = 0
        ListViewKoleksi.BeginUpdate()

        Dim i, jmlKoleksiKembali As Integer

        For i = ListViewKoleksi.Items.Count - 1 To 0 Step -1
            If ListViewKoleksi.Items(i).Checked = True Then
                idtraksaksikembali = ListViewKoleksi.Items.Item(i).SubItems(0).Text
                idkoleksikembali = ListViewKoleksi.Items.Item(i).SubItems(1).Text

                sql1 = "update TDetilTransaksi set TglPengembalian = '" & tglkembali & "' where IDTransaksi = '" & idtraksaksikembali & "' and IDKoleksi = '" & idkoleksikembali & "'"

                perintah.Connection = koneksiSQL
                perintah.CommandText = sql1
                perintah.ExecuteNonQuery()

                subDK = Val(ListViewKoleksi.Items.Item(i).SubItems(6).Text)
                totDK = totDK + subDK

                jmlKoleksiKembali = jmlKoleksiKembali + 1

                sql2 = "update TTransaksi set TotDenda = '" & totDK & "' where IDTransaksi = '" & idtraksaksikembali & "'"
                perintah.Connection = koneksiSQL
                perintah.CommandText = sql2
                perintah.ExecuteNonQuery()

                ListViewKoleksi.Items.RemoveAt(i)
            End If
        Next i
        ListViewKoleksi.EndUpdate()
        txt_totaldenda.Text = totaldenda + totDK

        koneksiSQL.Close()

        MsgBox("Anda mengembalikan " & jmlKoleksiKembali & " koleksi, dengan denda sebesar : Rp. " & totDK, MsgBoxStyle.Information, "Informasi")
    End Sub
End Class