﻿Imports System.Data.SqlClient
Public Class FormLokasi

    Private Sub FormLokasi_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim perintah As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        perintah.CommandText = "select * from TLokasi"

        da.SelectCommand = perintah
        da.Fill(ds, "tlokasi")

        dgv_lokasi.DataSource = ds
        dgv_lokasi.DataMember = "tlokasi"
        dgv_lokasi.ReadOnly = True
        'DGVAnggota.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        'DGVAnggota.AutoResizeColumn()
        dgv_lokasi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

        koneksiSQL.Close()
    End Sub

    Private Sub txt_idlokasi_Leave(sender As Object, e As EventArgs) Handles txt_idlokasi.Leave
        Dim idlokasi As String
        Dim perintah As New SqlCommand

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        idlokasi = txt_idlokasi.Text

        perintah.CommandText = "select IDLokasi from TLokasi where IDLokasi = '" & idlokasi & "'"

        Dim dr As SqlDataReader
        dr = perintah.ExecuteReader()

        If idlokasi = "" Then
            cekid.Checked = False
        Else
            If dr.Read Then
                cekid.Checked = False
                MsgBox("ID tersebut sudah ada", MsgBoxStyle.Critical, "Peringatan")
                txt_idlokasi.Focus()
            Else
                cekid.Checked = True
            End If
        End If
    End Sub

    Private Sub dgv_lokasi_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_lokasi.CellClick
        Dim i As Integer

        i = dgv_lokasi.CurrentRow.Index
        txt_idlokasi.Text = Trim(dgv_lokasi.Item(0, i).Value)
        txt_namalokasi.Text = Trim(dgv_lokasi.Item(1, i).Value)
        txt_tempat.Text = Trim(dgv_lokasi.Item(2, i).Value)

        btn_insert.Enabled = False
        btn_clear.Enabled = True
        btn_update.Enabled = True
        btn_delete.Enabled = True
    End Sub

    Private Sub btn_insert_Click(sender As Object, e As EventArgs) Handles btn_insert.Click
        Dim idlokasi, namalokasi, tempat As String
        Dim perintah As New SqlCommand

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        idlokasi = txt_idlokasi.Text
        namalokasi = txt_namalokasi.Text
        tempat = txt_tempat.Text

        perintah.CommandText = "insert into TLokasi values ('" & idlokasi & "', '" & namalokasi & "', '" & tempat & "')"
        perintah.ExecuteNonQuery()

        MsgBox("Data telah disimpan", MsgBoxStyle.Information, "Informasi")
        resetField()
        FormLokasi_Load(Me, New System.EventArgs)

        koneksiSQL.Close()

        btn_insert.Enabled = True
        btn_clear.Enabled = True
        btn_update.Enabled = False
        btn_delete.Enabled = False
    End Sub

    Private Sub resetField()
        txt_idlokasi.Text = ""
        txt_namalokasi.Text = ""
        txt_tempat.Text = ""

        btn_insert.Enabled = True
        btn_clear.Enabled = True
        btn_update.Enabled = False
        btn_delete.Enabled = False
    End Sub

    Private Sub btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click
        resetField()
    End Sub
End Class