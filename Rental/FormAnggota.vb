﻿Imports System.Data.SqlClient
Imports System.IO

Public Class FormAnggota

    Private Sub FormAnggota_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim perintah As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        perintah.CommandText = "select * from TAnggota"

        da.SelectCommand = perintah
        da.Fill(ds, "tanggota")

        dgv_anggota.DataSource = ds
        dgv_anggota.DataMember = "tanggota"
        dgv_anggota.ReadOnly = True
        'DGVAnggota.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        'DGVAnggota.AutoResizeColumn()
        dgv_anggota.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        PBFoto.ImageLocation = ("F:\Tasks\VB Project\Rental\image\default.png")
        PBFoto.SizeMode = PictureBoxSizeMode.StretchImage

        koneksiSQL.Close()
    End Sub

    Private Sub txt_idanggota_Leave(sender As Object, e As EventArgs) Handles txt_idanggota.Leave
        Dim idanggota As String
        Dim perintah As New SqlCommand

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        idanggota = txt_idanggota.Text

        perintah.CommandText = "select IDAnggota from TAnggota where IDAnggota = '" & idanggota & "'"

        Dim dr As SqlDataReader
        dr = perintah.ExecuteReader()

        If idanggota = "" Then
            cekid.Checked = False
        Else
            If dr.Read Then
                cekid.Checked = False
                MsgBox("ID tersebut sudah ada", MsgBoxStyle.Critical, "Peringatan")
                txt_idanggota.Focus()
            Else
                cekid.Checked = True
            End If
        End If
    End Sub

    Private Sub btn_insert_Click(sender As Object, e As EventArgs) Handles btn_insert.Click
        Dim idanggota, namaanggota, alamat, nohp As String
        Dim perintah As New SqlCommand

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        idanggota = txt_idanggota.Text
        namaanggota = txt_namaanggota.Text
        alamat = txt_alamat.Text
        nohp = txt_nohp.Text

        perintah.CommandText = "insert into TAnggota values ('" & idanggota & "', '" & namaanggota & "', '" & alamat & "', @gambar, '" & nohp & "')"
        Dim ms As New MemoryStream
        PBFoto.Image.Save(ms, PBFoto.Image.RawFormat)

        perintah.Parameters.Add("@gambar", SqlDbType.Image).Value = ms.ToArray()

        perintah.ExecuteNonQuery()

        MsgBox("Data telah disimpan", MsgBoxStyle.Information, "Informasi")
        resetField()
        FormAnggota_Load(Me, New System.EventArgs)

        koneksiSQL.Close()

        btn_insert.Enabled = True
        btn_clear.Enabled = True
        btn_update.Enabled = False
        btn_delete.Enabled = False
    End Sub

    Private Sub resetField()
        txt_idanggota.Text = ""
        txt_namaanggota.Text = ""
        txt_alamat.Text = ""
        txt_nohp.Text = ""

        btn_insert.Enabled = True
        btn_clear.Enabled = True
        btn_update.Enabled = False
        btn_delete.Enabled = False
    End Sub

    Private Sub dgv_anggota_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_anggota.CellClick
        Dim i As Integer

        i = dgv_anggota.CurrentRow.Index
        txt_idanggota.Text = Trim(dgv_anggota.Item(0, i).Value)
        txt_namaanggota.Text = Trim(dgv_anggota.Item(1, i).Value)
        txt_alamat.Text = Trim(dgv_anggota.Item(2, i).Value)
        txt_nohp.Text = Trim(dgv_anggota.Item(4, i).Value)

        btn_insert.Enabled = False
        btn_clear.Enabled = True
        btn_update.Enabled = True
        btn_delete.Enabled = True
    End Sub

    Private Sub btn_update_Click(sender As Object, e As EventArgs) Handles btn_update.Click
        Dim idanggota, namaanggota, alamat, nohp As String
        Dim hasilpesan As String
        Dim perintah As New SqlCommand

        idanggota = txt_idanggota.Text
        namaanggota = txt_namaanggota.Text
        alamat = txt_alamat.Text
        nohp = txt_nohp.Text

        bariske = dgv_anggota.CurrentRow.Index
        idedit = dgv_anggota.Item(0, bariske).Value


        hasilpesan = MsgBox("Apakah anda yakin akan mengedit " & Trim(idedit) & "?", MsgBoxStyle.YesNo, "Perhatian")
        If hasilpesan = vbYes Then
            koneksiSQL = AmbilKoneksi()
            koneksiSQL.Open()

            perintah = koneksiSQL.CreateCommand
            perintah.CommandText = "update TAnggota set Nama = '" & namaanggota & "', Alamat = '" & alamat & "', Foto = @gambar, HP = '" & nohp & "' where IDAnggota = '" & idedit & "'"
            Dim ms As New MemoryStream

            PBFoto.Image.Save(ms, PBFoto.Image.RawFormat)
            perintah.Parameters.Add("@gambar", SqlDbType.Image).Value = ms.ToArray
            perintah.ExecuteNonQuery()
            resetField()

            koneksiSQL.Close()
            FormAnggota_Load(Me, New System.EventArgs)

        End If
    End Sub

    Private Sub btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click
        resetField()
    End Sub

    Private Sub btn_pilihfoto_Click(sender As Object, e As EventArgs) Handles btn_pilihfoto.Click
        ODBukaFoto.Filter = "All file (*.*)|*.*| JPG Files(*.jpg)|*.jpg|JPEG Files (*.jpg)|*.jpeg|PNG Files(*.png)|*.png|GIF Files(*.gif)|*.gif"
        Try
            If ODBukaFoto.ShowDialog = Windows.Forms.DialogResult.OK Then
                PBFoto.Image = New Bitmap(ODBukaFoto.FileName)
                PBFoto.SizeMode = PictureBoxSizeMode.StretchImage
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub dgv_anggota_Paint(sender As Object, e As PaintEventArgs) Handles dgv_anggota.Paint
        Dim jmlbaris As Integer
        Dim idanggota As String

        jmlbaris = dgv_anggota.RowCount - 1
        bariske = dgv_anggota.CurrentCell.RowIndex

        If bariske < jmlbaris Then
            idanggota = dgv_anggota.Item(0, bariske).Value

            Dim perintah As New SqlCommand
            Dim data As SqlDataReader

            koneksiSQL = AmbilKoneksi()
            koneksiSQL.Open()

            perintah = koneksiSQL.CreateCommand
            perintah.CommandText = "select Foto from TAnggota where IDAnggota = '" & idanggota & "'"

            data = perintah.ExecuteReader
            data.Read()

            Dim myfoto As Byte()
            If Not IsDBNull(data(0)) Then
                myfoto = data(0)
                PBFoto.Image = Image.FromStream(New IO.MemoryStream(myfoto))
                PBFoto.SizeMode = PictureBoxSizeMode.StretchImage
            End If
            koneksiSQL.Close()
        Else
            PBFoto.ImageLocation = ("F:\Tasks\VB Project\Rental\image\default.png")
            PBFoto.Load()
        End If
    End Sub
End Class