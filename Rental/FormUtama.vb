﻿Public Class FormUtama

    Private Sub UserToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UserToolStripMenuItem.Click
        Dim formUser As New FormUser
        formUser.Show()
        formUser.MdiParent = Me
    End Sub

    Private Sub AnggotaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AnggotaToolStripMenuItem.Click
        Dim formAnggota As New FormAnggota
        formAnggota.Show()
        formAnggota.MdiParent = Me
    End Sub

    Private Sub KoleksiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KoleksiToolStripMenuItem.Click
        Dim formKoleksi As New FormKoleksi
        formKoleksi.Show()
        formKoleksi.MdiParent = Me
    End Sub

    Private Sub LokasiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LokasiToolStripMenuItem.Click
        Dim formLokasi As New FormLokasi
        formLokasi.Show()
        formLokasi.MdiParent = Me
    End Sub

    Private Sub PeminjamanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PeminjamanToolStripMenuItem.Click
        Dim formpeminjaman As New FormTransaksi
        formpeminjaman.Show()
        formpeminjaman.MdiParent = Me
    End Sub

    Private Sub PengembalianToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PengembalianToolStripMenuItem.Click
        Dim formpengembalian As New FormPengembalian
        formpengembalian.Show()
        formpengembalian.MdiParent = Me
    End Sub

    Private Sub LaporanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LaporanToolStripMenuItem.Click
        Dim formlaporanpinjam As New LaporanPinjam
        formlaporanpinjam.Show()
        formlaporanpinjam.MdiParent = Me
    End Sub
End Class
