﻿Imports System.Data.SqlClient
Public Class FormKoleksi

    Private Sub FormKoleksi_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim perintah As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        isicombolokasi()
        perintah.CommandText = "select * from TKoleksi"

        da.SelectCommand = perintah
        da.Fill(ds, "tkoleksi")

        dgv_koleksi.DataSource = ds
        dgv_koleksi.DataMember = "tkoleksi"
        dgv_koleksi.ReadOnly = True
        'DGVAnggota.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        'DGVAnggota.AutoResizeColumn()
        dgv_koleksi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

        koneksiSQL.Close()
    End Sub

    Private Sub txt_idkoleksi_Leave(sender As Object, e As EventArgs) Handles txt_idkoleksi.Leave
        Dim idkoleksi As String
        Dim perintah As New SqlCommand

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        idkoleksi = txt_idkoleksi.Text

        perintah.CommandText = "select IDKoleksi from TKoleksi where IDKoleksi = '" & idkoleksi & "'"

        Dim dr As SqlDataReader
        dr = perintah.ExecuteReader()

        If idkoleksi = "" Then
            cekid.Checked = False
        Else
            If dr.Read Then
                cekid.Checked = False
                MsgBox("ID tersebut sudah ada", MsgBoxStyle.Critical, "Peringatan")
                txt_idkoleksi.Focus()
            Else
                cekid.Checked = True
            End If
        End If
    End Sub

    Private Sub btn_insert_Click(sender As Object, e As EventArgs) Handles btn_insert.Click
        Dim idkoleksi, idlokasi, judul, jenis As String
        Dim jumlah, jangkawaktu, biayasewa As Integer
        Dim perintah As New SqlCommand

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()
        perintah = koneksiSQL.CreateCommand

        idkoleksi = txt_idkoleksi.Text
        idlokasi = cb_idlokasi.SelectedItem
        judul = txt_judul.Text
        jenis = cb_jenis.SelectedItem
        jumlah = txt_jumlah.Text
        jangkawaktu = txt_jangkawaktu.Text
        biayasewa = txt_biayasewa.Text

        perintah.CommandText = "insert into TKoleksi values ('" & idkoleksi & "', '" & judul & "', '" & jenis & "', '" & jumlah & "', '" & jangkawaktu & "', '" & biayasewa & "', '" & idlokasi & "')"
        perintah.ExecuteNonQuery()

        MsgBox("Data telah disimpan", MsgBoxStyle.Information, "Informasi")
        resetField()
        FormKoleksi_Load(Me, New System.EventArgs)

        koneksiSQL.Close()

        btn_insert.Enabled = True
        btn_clear.Enabled = True
        btn_update.Enabled = False
        btn_delete.Enabled = False
    End Sub

    Private Sub resetField()
        txt_idkoleksi.Enabled = True
        txt_idkoleksi.Text = ""
        cb_idlokasi.SelectedItem = Nothing
        txt_judul.Text = ""
        cb_jenis.SelectedItem = Nothing
        txt_jumlah.Text = ""
        txt_jangkawaktu.Text = ""
        txt_biayasewa.Text = ""

        btn_insert.Enabled = True
        btn_clear.Enabled = True
        btn_update.Enabled = False
        btn_delete.Enabled = False
    End Sub

    Private Sub btn_clear_Click(sender As Object, e As EventArgs) Handles btn_clear.Click
        resetField()
    End Sub

    Private Sub isicombolokasi()
        Dim da As New SqlDataAdapter("select IDLokasi from TLokasi", koneksiSQL)
        Dim dt As New DataTable

        koneksiSQL = AmbilKoneksi()
        koneksiSQL.Open()

        da.Fill(dt)
        cb_idlokasi.DisplayMember = "idlokasi"
        cb_idlokasi.DataSource = dt

        koneksiSQL.Close()
    End Sub

    Private Sub dgv_koleksi_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_koleksi.CellClick
        Dim i As Integer

        i = dgv_koleksi.CurrentRow.Index
        txt_idkoleksi.Text = Trim(dgv_koleksi.Item(0, i).Value)
        cb_idlokasi.Text = Trim(dgv_koleksi.Item(1, i).Value)
        txt_judul.Text = Trim(dgv_koleksi.Item(2, i).Value)
        cb_jenis.Text = Trim(dgv_koleksi.Item(3, i).Value)
        txt_jumlah.Text = Trim(dgv_koleksi.Item(4, i).Value)
        txt_jangkawaktu.Text = Trim(dgv_koleksi.Item(5, i).Value)
        txt_biayasewa.Text = Trim(dgv_koleksi.Item(6, i).Value)

        btn_insert.Enabled = False
        btn_clear.Enabled = True
        btn_update.Enabled = True
        btn_delete.Enabled = True
    End Sub
End Class